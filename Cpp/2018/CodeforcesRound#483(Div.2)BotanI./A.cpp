#include <bits/stdc++.h>
using namespace std;

#define mb make_pair;
#define pb push_back;

typedef long long int ll;
typedef long double ld;

const int INF = 1e9+7;
const ll LINF = 1e18+3;
const int L   = 100*1000+256;
const int XL  = 1000*1000+128;

int a[L];

int main()
{
      int n;
      cin>>n;
      for(int i=0;i<n;i++)
      {
            cin>>a[i];
      }
      sort(a,a+n);
      cout<<a[((n+1)/2)-1];
      return 0;
}
