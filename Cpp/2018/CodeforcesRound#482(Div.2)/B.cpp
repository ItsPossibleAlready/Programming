#include<bits/stdc++.h>
using namespace std;

#define mb make_pair;
#define pb push_back;

typedef long long int ll;
typedef long double ld;
const int INF = 1e9+7;
const ll LINF = 1e18+3;
const int L   = 100*1000+256;
const int XL  = 1000*1000+128;
string q[5];
int p[1000];
int n;
int func(string s)
{
    for(int i=0;i<s.size();i++)
    {
        p[s[i]]++;
    }
    int mx=0;
    for(int i=0;i<250;i++)
    {
      mx=max(p[i],mx);
      p[i]=0;
    }
    if(mx==s.size()&&n==1)return s.size()-1;
    return min(n+mx,int(s.size()));
}

int main()
{
    cin>>n;
    cin>>q[0]>>q[1]>>q[2];
    int ans1=func(q[0]);
    int ans2=func(q[1]);
    int ans3=func(q[2]);
    if(ans1>ans2&&ans1>ans3)
    {
        cout<<"Kuro";
    }
    else if(ans2>ans1&&ans2>ans3)
    {
        cout<<"Shiro";
    }
    else if(ans3>ans2&&ans3>ans1)
    {
        cout<<"Katie";
    }
    else
    {
        cout<<"Draw";
    }
}
