#include <bits/stdc++.h>
using namespace std;

#define mb make_pair;
#define pb push_back;

typedef long long int ll;
typedef long double ld;

const int INF = 1e9+7;
const ll LINF = 1e18+3;
const int L   = 100*1000+256;
const int XL  = 1000*1000+128;
int p;
vector<int> graph[L];
int note[L];

void depthfirstsearch(int currentnode)
{
      note[currentnode] = true;
      for(int i=0;i<graph[currentnode].size();i++)
      {
            int to = graph[currentnode][i];
            if(note[to])
            {
                  depthfirstsearch(to);
            }
      }
      return;
}

int main()
{
      /*Reading Graph*/
      depthfirstsearch(1);
}
