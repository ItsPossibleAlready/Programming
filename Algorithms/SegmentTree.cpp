#include <bits/stdc++.h>
using namespace std;

#define mb make_pair;
#define pb push_back;

typedef long long int ll;
typedef long double ld;

const int INF = 1e9+7;
const ll LINF = 1e18+3;
const int L   = 100*1000+256;
const int XL  = 1000*1000+128;

int massive [L];
int segmentTree [L];
int massivesize;

void build(int treeIterator,int treeLeft,int treeRight)
{
      if(treeLeft == treeRight)
      {
            segmentTree [treeIterator] = massive [treeLeft];
      }
      else
      {
            int mid = (treeLeft + treeRight) /2;
            build(treeIterator * 2,treeLeft , mid );
            build(treeIterator*2+1,mid+1,treeRight);
            segmentTree [treeIterator] = segmentTree [treeIterator*2] + segmentTree [treeIterator*2+1];
      }
}

int sum(int treeIterator,int treeLeft,int treeRight,int findLeft,int findRight)
{
      if(findLeft>findRight)return 0;
      if(treeLeft == findLeft && treeRight == findRight)
      {
            return segmentTree[treeIterator];
      }
            int mid = (treeLeft + treeRight) /2;
            return sum(treeIterator*2,treeLeft,mid,findLeft,min(mid,findRight))+sum(treeIterator*2+1,mid+1,treeRight,max(mid,findLeft),findRight);
}
void update(int treeIterator,int treeLeft,int treeRight,int positionUpdate,int valueUpdate)
{
      if(positionUpdate>=treeLeft && positionUpdate<=treeRight)return;

      if(treeLeft==treeRight)
      {
            segmentTree[treeIterator] = valueUpdate;
            return;
      }
            int mid = (treeLeft + treeRight) /2;
            update(treeIterator * 2, treeLeft ,mid ,positionUpdate,valueUpdate);
            update(treeIterator*2+1,mid+1,treeRight,positionUpdate,valueUpdate);
            segmentTree[treeIterator] = segmentTree[treeIterator*2]+segmentTree[treeIterator*2+1];
}
int main()
{
      /*reading massive*/
      build(1,1,massivesize);
}
